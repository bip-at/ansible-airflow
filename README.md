Role Name
=========

Setup an airflow installation based on an git distribution based on pipenv


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - airflow
          vars:
            airflow_dist: https://....
            airflow_key: 
            airflow_smtp_server:
            ...

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
